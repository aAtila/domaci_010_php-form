<?php

// Check for empty fields

    $link = $gmail = "";

	if (empty($_POST['name'])) {
        $link = '&nameErr=1';
      } 
      
    if (empty($_POST['email'])) {
        $link .= '&emailErr=1';
      } 
      
    if (empty($_POST['website'])) {
        $link .= '&websiteErr=1';
      } 
      
    if (empty($_POST['bday'])) {
        $link .= '&bdayErr=1';
      } 
    
    if (empty($_POST['biography'])) {
        $link .= '&biographyErr=1';
    }

// Check name and email length

    if (strlen($_POST['name']) < 2) {
        $link .= '&lennameErr=1';        
    }

    if (strlen($_POST['email']) < 16) {
        $link .= '&lenmailErr=1';        
    }

// Check if the email is gmail

    //   if (strpos($email, '@gmail.com') == false) {
    //     $link .= '&gmailErr=1';
    //     $gmail = true;
    //  }

     if (substr($email, 0, -10) !== "@gmail.com") {
        $link .= '&gmailErr=1';
     }

// Function checkEmailAdress

     function checkEmailAdress($email) {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $emailInvalid = "Invalid email format"; 
          }
     }

// Clean up data

    $name = $email = $website = $bday = $biography = "";
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$name = test_input($_POST["name"]);
		$email = test_input($_POST["email"]);
		$website = test_input($_POST["website"]);
        $bday = test_input($_POST["bday"]);
        $biography = test_input($_POST["biography"]);

	}

	function test_input($data) {
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}

// Save entered data    

    $link .= "&name=$name&email=$email&website=$website&bday=$bday";

 
    if (empty($_POST['name']) or empty($_POST['website']) or empty($_POST['email']) or empty($_POST['website']) or empty($_POST['bday']) or empty($_POST['biography'])) {
        header("Location: index.php?$link");
    }
      
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="PHP domaci - funkcije i varijabli.">
        <meta name="keywords" content="php, functions, variables">
        <meta name="author" content="Atila Alacan">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>PHP domaci - funkcije i varijabli</title>
        <link href="https://fonts.googleapis.com/css?family=Fira+Mono|Fira+Sans" rel="stylesheet">
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/styles.css">
    </head>
    <body>
        <nav></nav>
        <main>
            <h2>Thank You!</h2>



            <p>Welcome <?php echo $name; ?></p>
            <p>Your email address is <?php echo $email; ?></p>
            <p>Your website is <?php echo $website; ?></p>
            <p>Your birthday is on <?php echo $bday; ?>. You were born on a lovely <?php echo date('l', strtotime($bday)); ?>.</p>
            <p>Your biography: <?php echo $biography; ?></p>
        </main>
    </body>
</html>