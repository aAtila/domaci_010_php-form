<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="PHP domaci - funkcije i varijabli.">
        <meta name="keywords" content="php, functions, variables">
        <meta name="author" content="Atila Alacan">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>PHP domaci - funkcije i varijabli</title>
        <link href="https://fonts.googleapis.com/css?family=Fira+Mono|Fira+Sans" rel="stylesheet">
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/styles.css">
    </head>
    <body>
        <nav></nav>
        <main>
            <h2>Registration Form</h2>



            <p class="code--int"><?php if (isset($_GET['error'])) { echo '* All fields are required!'; } ?></p>
            <div class="form">
                <form action="register.php" method="post">
                <lable>Name</lable><br>
                <input type="text" name="name" value="<?php if (isset($_GET['name'])) { echo $_GET['name']; } ?>">
                <span class="code--int"><?php if (isset($_GET['lennameErr'])) { echo 'Name too short!'; } ?></span>
                <span class="code--int"><?php if (isset($_GET['nameErr'])) { echo 'Field missing!'; } ?></span><br><br>
                <lable>Email</lable><br>
                <input type="text" name="email" value="<?php if (isset($_GET['email'])) { echo $_GET['email']; } ?>">
                <span class="code--int"><?php if (isset($_GET['lenmailErr'])) { echo 'Email address too short! '; } ?></span>
                <span class="code--int"><?php if (isset($_GET['gmailErr'])) { echo 'Not a valid gmail address! '; } ?></span>
                <span class="code--int"><?php if (isset($_GET['emailErr'])) { echo 'Field missing!'; } ?></span><br><br>
                <lable>Website</lable><br>
                <input type="text" name="website" value="<?php if (isset($_GET['website'])) { echo $_GET['website']; } ?>">
                <span class="code--int"><?php if (isset($_GET['websiteErr'])) { echo 'Field missing!'; } ?></span><br><br>
                <lable>Date of Birth</lable><br>        
                <input type="date" name="bday" value="<?php if (isset($_GET['bday'])) { echo $_GET['bday']; } ?>">
                <span class="code--int"><?php if (isset($_GET['bdayErr'])) { echo 'Field missing!'; } ?></span><br><br>
                <label>Biography</label><br>
                <textarea name="biography" rows="10" cols="50"></textarea>
                <span class="code--int"><?php if (isset($_GET['biographyErr'])) { echo 'Field missing!'; } ?></span><br><br>
                <input type="submit">
                </form>
            </div>
        </main>
    </body>
</html>